require("dotenv").config();
const fetch = require("node-fetch");
const http = require("https");
const connection = require("./dbConnection"); // Connection to database

const config = {
	database: process.env.DB_NAME,
	host: process.env.DB_ENDPOINT,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	port: process.env.DB_PORT,
	timeout: 60000,
};

const apiUrl = process.env.PS_API_URL || "";
const apiToken = `Bearer ${process.env.PS_TOKEN}`;
const httpAgent = new http.Agent({
	rejectUnauthorized: false,
});

var CONFIG = {
	method: "POST",
	agent: httpAgent,
	headers: {
		"Content-Type": "application/json",
		Authorization: apiToken,
	},
};

async function fetchAPI(url, body = null) {
	if (body) CONFIG = { ...CONFIG, body: JSON.stringify(body) };
	const res = await fetch(url, CONFIG);
	return res.json();
}

function varcharOrNumber(value, type) {
	if (value == undefined) return "null";
	if (type == "int") return Number(value);
	return JSON.stringify(value);
}

async function asyncForEach(array, callback) {
	for (let index = 0; index < array.length; index++) {
		await callback(array[index], index, array);
	}
}

module.exports = {
	/**
	 * All of the contacts json data, from apiCall, matches the table columns
	 * and will be directly inserted into the contacts table
	 *
	 * Table Structure:
	 * - studentId					varchar(20)
	 * - contactId					int(10)
	 * - firstName					VARCHAR(10)
	 * - lastName						VARCHAR(10)
	 * - relationship				VARCHAR(50)
	 * - emailAddress				VARCHAR(50)
	 * - homePhone					VARCHAR(20)
	 * - mobilePhone				VARCHAR(20)
	 * - street							VARCHAR(50)
	 * - city								VARCHAR(50)
	 * - state							VARCHAR(50)
	 * - zip								VARCHAR(20)
	 * - guardianIndicator	int(1)
	 * - emergencyIndicator	int(1)
	 * - priority						int(2)
	 */
	contacts: async () => {
		const resp = await fetchAPI(`${apiUrl}/com.welfie.queries.contacts`); // ?pagesize=0 to pull all records
		const db = connection(config);
		let completed = 0;
		let errorLogs = [];
		console.log("Triggered lambda function for contacts");

		await asyncForEach(resp.record, async (r, i) => {
			r.array = [
				varcharOrNumber(r.studentid),
				varcharOrNumber(r.contactid),
				varcharOrNumber(r.firstname),
				varcharOrNumber(r.lastname),
				varcharOrNumber(r.relationship),
				varcharOrNumber(r.emailaddress),
				varcharOrNumber(r.homephone),
				varcharOrNumber(r.mobilephone),
				varcharOrNumber(r.street),
				varcharOrNumber(r.city),
				varcharOrNumber(r.state),
				varcharOrNumber(r.zip),
				varcharOrNumber(r.guardianindicator),
				varcharOrNumber(r.emergencyindicator),
				varcharOrNumber(r.priority),
			];

			const query = `INSERT INTO contacts (studentId, contactId, firstName, lastName, relationship, emailAddress, homePhone, mobilePhone, street, city, state, zip, guardianIndicator, emergencyIndicator, priority) VALUES (${r.array.join(
				", "
			)})`;

			try {
				await db.query(query);
				completed++;
			} catch (error) {
				errorLogs.push(error.sqlMessage);
			}
		});

		console.log("Contacts MySQL Row completed:", completed);
		console.log("Contacts MySQL Error(s):", errorLogs);
		await db.close();

		resp.errors = errorLogs;
		resp.apiName = "Contacts";

		return resp;
	},

	/**
	 * Most of the users json data, from apiCall, matches the table columns
	 * and will be directly inserted into the users table
	 *
	 * Properties that were not found in the apiCall during testing
	 * but are in the table requested:
	 * - staffID
	 *
	 * Table Structure:
	 * userId				int (10)
	 * status				varchar(50)
	 * enabledUser	varchar(50)
	 * schoolId			int (10)
	 * role					varchar(50)
	 * givenName		varchar(50)
	 * familyName		varchar(50)
	 * middleName		varchar(50)
	 * studentId		varchar(20)
	 * staffId			int(10)
	 * email				varchar(100)
	 * phone				varchar(20)
	 */
	users: async () => {
		const resp = await fetchAPI(`${apiUrl}/com.welfie.queries.users`);
		const db = connection(config);
		let completed = 0;
		let errorLogs = [];
		console.log("Triggered lambda function for users");

		await asyncForEach(resp.record, async (r, i) => {
			r.array = [
				varcharOrNumber(r.userid),
				varcharOrNumber(r.status),
				varcharOrNumber(r.enableduser),
				varcharOrNumber(r.schoolid),
				varcharOrNumber(r.role),
				varcharOrNumber(r.givenname),
				varcharOrNumber(r.familyname),
				varcharOrNumber(r.middlename),
				varcharOrNumber(r.studentid),
				varcharOrNumber(r.staffid),
				varcharOrNumber(r.email),
				varcharOrNumber(r.phone),
			];

			const query = `INSERT INTO users (userId, status, enabledUser, schoolId, role, givenName, familyName, middleName, studentId, staffId, email, phone) VALUES (${r.array.join(
				", "
			)})`;

			try {
				await db.query(query);
				completed++;
			} catch (error) {
				errorLogs.push(error.sqlMessage);
			}
		});

		console.log("Users MySQL Row completed:", completed);
		console.log("Users MySQL Error(s):", errorLogs);
		await db.close();

		resp.errors = errorLogs;
		resp.apiName = "Users";

		return resp;
	},

	/**
	 * Most of the students json data, from apiCall, matches the table columns
	 * and will be directly inserted into the students table
	 *
	 * Properties that were not found in the apiCall during testing
	 * but are in the table requested:
	 * - countryOfBirthAbbreviation
	 * - stateOfBirthAbbreviation
	 * - cityOfBirth
	 * - publicSchoolResidenceStatus
	 *
	 * Table Structure:
	 * - studentId														varchar(20)
	 * - status																int(2)
	 * - birthDate														varchar(50)
	 * - sex																	varchar(1)
	 * - americanIndianOrAlaskaNative					Boolean
	 * - asian																Boolean
	 * - blackOrAfricanAmerican								Boolean
	 * - nativeHawaiianOrOtherPacificIslander	Boolean
	 * - white																Boolean
	 * - demographicRaceTwoOrMoreRaces				Boolean
	 * - hispanicOrLatinoEthnicity						Boolean
	 * - countryOfBirthAbbreviation						varchar(4)
	 * - stateOfBirthAbbreviation							varchar(4)
	 * - cityOfBirth													varchar(50)
	 * - publicSchoolResidenceStatus					varchar(50)
	 */
	students: async () => {
		const resp = await fetchAPI(`${apiUrl}/com.welfie.queries.demographics`);
		const db = connection(config);
		let completed = 0;
		let errorLogs = [];
		console.log("Triggered lambda function for Students");

		await asyncForEach(resp.record, async (r, i) => {
			r.array = [
				varcharOrNumber(r.studentid),
				varcharOrNumber(r.status, "int"),
				varcharOrNumber(r.birthdate),
				varcharOrNumber(r.sex),
				varcharOrNumber(r.americanindianoralaskanative),
				varcharOrNumber(r.asian),
				varcharOrNumber(r.blackorafricanamerican),
				varcharOrNumber(r.nativehawaiianorotherpacificislander),
				varcharOrNumber(r.white),
				varcharOrNumber(r.demographicracetwoormoreraces),
				varcharOrNumber(r.hispanicOrLatinoEthnicity),
				varcharOrNumber(r.countryofbirthabbreviation),
				varcharOrNumber(r.stateofbirthabbreviation),
				varcharOrNumber(r.cityofbirth),
				varcharOrNumber(r.publicschoolresidencestatus),
			];

			const query = `INSERT INTO students (studentId, status, birthDate, sex, americanIndianOrAlaskaNative, asian, blackOrAfricanAmerican, nativeHawaiianOrOtherPacificIslander, white, demographicRaceTwoOrMoreRaces, hispanicOrLatinoEthnicity, countryOfBirthAbbreviation, stateOfBirthAbbreviation, cityOfBirth, publicSchoolResidenceStatus) VALUES (${r.array.join(
				", "
			)})`;

			try {
				await db.query(query);
				completed++;
			} catch (error) {
				errorLogs.push(error.sqlMessage);
			}
		});

		console.log("Students MySQL Row completed:", completed);
		console.log("Students MySQL Error(s):", errorLogs);
		await db.close();

		resp.errors = errorLogs;
		resp.apiName = "Students";

		return resp;
	},

	/**
	 * Attendance apiCall did not have any data during testing, but the json
	 * will most likely match the columns similar to the previous functions
	 *
	 * Table Structure:
	 * - attendanceDcid			int(10)
	 * - attendanceId				int(15)
	 * - attendanceCodeId		int
	 * - calendarDayId			int
	 * - schoolId						int
	 * - yearId							int
	 * - studentId					varchar
	 * - ccid								int
	 * - periodId						int
	 * - sectionId					int
	 * - attComment					varchar(150)
	 * - attDate						varchar(50)
	 * - attCode						varchar(10)
	 * - attCodeDescription	varchar(50)
	 * - presenceStatusCode	varchar(50)
	 */
	attendance: async (req) => {
		const resp = await fetchAPI(`${apiUrl}/com.welfie.queries.attendance`, {
			attDate: req.query.date,
		});
		const db = connection(config);
		let completed = 0;
		let errorLogs = [];
		console.log("Triggered lambda function for Attendance");

		if (!resp.record) return resp;

		await asyncForEach(resp.record, async (r, i) => {
			r.array = [
				varcharOrNumber(r.attendancedcid, "int"),
				varcharOrNumber(r.attendanceid, "int"),
				varcharOrNumber(r.attendancecodeid, "int"),
				varcharOrNumber(r.calendardayid, "int"),
				varcharOrNumber(r.schoolid, "int"),
				varcharOrNumber(r.yearid, "int"),
				varcharOrNumber(r.studentid),
				varcharOrNumber(r.ccid, "int"),
				varcharOrNumber(r.periodid, "int"),
				varcharOrNumber(r.sectionid, "int"),
				varcharOrNumber(r.attcomment),
				varcharOrNumber(r.attdate),
				varcharOrNumber(r.attcode),
				varcharOrNumber(r.attcodedescription),
				varcharOrNumber(r.presencestatuscode),
			];

			const query = `INSERT INTO attendance (attendanceDcid, attendanceId, attendanceCodeId, calendarDayId, schoolId, yearId, studentId, ccid, periodId, sectionId, attComment, attDate, attCode, attCodeDescription, presenceStatusCode) VALUES (${r.array.join(
				", "
			)})`;

			try {
				await db.query(query);
				completed++;
			} catch (error) {
				errorLogs.push(error.sqlMessage);
			}
		});

		console.log("Attendance MySQL Row completed:", completed);
		console.log("Attendance MySQL Error(s):", errorLogs);
		await db.close();

		resp.errors = errorLogs;
		resp.apiName = "Attendance";

		return resp;
	},

	/**
	 * Schedules apiCall did not have any data during testing, but the json
	 * will most likely match the columns similar to the previous functions
	 *
	 * Table Structure:
	 * - studentId		varchar
	 * - staffId			int
	 * - room					varchar(50)
	 * - courseName		varchar(100)
	 * - date					varchar(50)
	 * - periodNumber	varchar(10)
	 * - sectionId		int
	 * - ccId					int
	 * - meetingTime	varchar(100)
	 */
	schedules: async (req) => {
		const resp = await fetchAPI(
			`${apiUrl}/com.welfie.queries.studentschedules`,
			{ date: req.query.date }
		);
		const db = connection(config);
		let completed = 0;
		let errorLogs = [];
		console.log("Triggered lambda function for student schedules");

		if (!resp.record) return resp;

		await asyncForEach(resp.record, async (r, i) => {
			r.array = [
				varcharOrNumber(r.studentid),
				varcharOrNumber(r.staffid, "int"),
				varcharOrNumber(r.room),
				varcharOrNumber(r.coursename),
				varcharOrNumber(r.date),
				varcharOrNumber(r.periodnumber),
				varcharOrNumber(r.sectionid, "int"),
				varcharOrNumber(r.ccid, "int"),
				varcharOrNumber(r.meetingtime),
			];

			const query = `INSERT INTO schedules (studentId, staffId, room, courseName, date, periodNumber, sectionId, ccId, meetingTime) VALUES (${r.array.join(
				", "
			)})`;

			try {
				await db.query(query);
				completed++;
			} catch (error) {
				errorLogs.push(error.sqlMessage);
			}
		});

		console.log("Schedules MySQL Row completed:", completed);
		console.log("Schedules MySQL Error(s):", errorLogs);
		await db.close();

		resp.errors = errorLogs;
		resp.apiName = "Schedules";

		return resp;
	},
};
