require("dotenv").config();
const util = require("util");
const mysql = require("mysql");

function makeDb(config) {
	const connection = mysql.createConnection(config);
	console.log("DB Connected");
	return {
		query(sql, args) {
			return util.promisify(connection.query).call(connection, sql, args);
		},
		close() {
			console.log("Connection Ended");
			return util.promisify(connection.end).call(connection);
		},
	};
}

module.exports = makeDb;
