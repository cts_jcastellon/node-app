const fetch = require("node-fetch");

const apiCalls = require("./apiCalls");
const connection = require("./dbConnection");

module.exports = (app) => {
	app.get("/", async (req, res) => {
		console.log("Triggered lambda function at scheduled event");
		/**
		 * TODO: Run all apiCalls functions here
		 *
		 * This is where all functions from apiCalls will run to
		 * insert data into the Aurora database.
		 */

		let date = new Date(),
			qyear = date.getFullYear(),
			qmonth =
				date.getMonth() < 9 ? `0${date.getMonth() + 1}` : date.getMonth() + 1,
			qday = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate(),
			qdate = `${qyear}-${qmonth}-${qday}`;

		req.query.date = qdate;

		var promises = await Promise.all([
			apiCalls.contacts(),
			apiCalls.users(),
			apiCalls.students(),
			apiCalls.attendance(req),
			apiCalls.schedules(req),
		]);

		res.send(promises);
	});

	// Cotacts
	app.get("/contacts", async (req, res) => res.send(await apiCalls.contacts()));

	// Users
	app.get("/users", async (req, res) => res.send(await apiCalls.users()));

	// Students
	app.get("/students", async (req, res) => res.send(await apiCalls.students()));

	// Attendance
	app.get("/attendance", async (req, res) =>
		res.send(await apiCalls.attendance(req))
	);

	// Student Schedules
	app.get("/schedules", async (req, res) =>
		res.send(await apiCalls.schedules(req))
	);
};
